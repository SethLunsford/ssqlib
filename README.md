#### This library was created to retrieve information from Source servers running any type of game.
### Features

1. Retrieve server information from any type of Source server
    * Server name
    * Current amount of players
    * Max players allowed on the server
    * Current amount of bots
    * Game version
    * Game name
    * Current map
    * Steam App ID for game being played on the server 

2. Retrieve player information from any type of Source server
    * Player index
    * Player's name
    * Kills
    * Time on the server 